function printId(id : number | string){
    console.log(`Id is ${id}`);
}

printId(34);

printId("344");

// ** narrowing

function printId2(id : number | string){
    if(typeof id === "string"){
        // safe to call string methods
        console.log(id.toUpperCase());
    }else{
        // else its a number
        console.log(id.toFixed(2));
    }
}

printId("3hjjuue");
printId(34)