export default function printSomething(values: { name: string; value: any }[]) {
  for (const val of values) {
    console.log(`${val.name} => ${val.value}`);
  }
}
