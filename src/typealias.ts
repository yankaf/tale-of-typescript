type num = number;

type Person = {
    name : string,
    age : number
}

type strNum = string | number;

// any type can be given to alias


function iTakeANumAsPeram(nu:num){
    console.log(nu.toFixed(3))
}

iTakeANumAsPeram(33);


function iTakeObjAsPeram(obj:Person){
    console.log(`Hi, I'm ${obj.name} and I'm ${obj.age} years old`);
    
}