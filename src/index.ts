// Local packages can be added just like other one 
// "examplemodule": "file:../../../examplemodule"

import printSomething from "./print.js";

interface Person {
  name: string;
  age: number;
}

function printName(name: string, date: Date, place: string): string {
  return `My name is ${name} and I was born on ${date.toDateString()}, in ${place}`;
}

let d: Date = new Date();
d.setDate(15);
d.setMonth(5);
d.setFullYear(2001);

printSomething([
  { name: "mystr", value: printName("Random Shot", d, "Place Name") },
  { name: "name", value: [1, 2, 3, 4, 5, 6, 7, 8] },
  { name: "myname", value: "Place Name" },
]);


function iWantAPerson(person:Person){
    console.log(person.name, person.age);
}

iWantAPerson({age : 23, name : "test val"});

// ! @Types 

let personNames : Array<number> = [];
personNames.push(2);
const age = 34;
let persons: Array<Person> = [];
persons.push({age : 23, name : "John Stevens"});
persons.push({name : "Steve Griffith", age});

for (const i of persons) {
  console.log(`${i.name}, ${i.age}`);
}



// ! Optional property in Object types

function iTakeObjAsPeram(obj :{name : string, marks : number, declareIt? : boolean}){
  console.log(obj.name, obj.marks); // Sure to be type they are written in
  console.log(obj.declareIt ? obj.declareIt : false);
  if(obj.declareIt !== undefined){
    console.log(!obj.declareIt);
  }
}

iTakeObjAsPeram(
  {
    name : "John Stevens",
    marks : 34
  }
)